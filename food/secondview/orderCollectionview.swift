//
//  orderCollectionview.swift
//  food
//
//  Created by Abhinand on 23/11/18.
//  Copyright © 2018 Abhinand. All rights reserved.
//

import UIKit

class orderCollectionview: UICollectionView ,UICollectionViewDataSource,UICollectionViewDelegate {
    struct order {
        var orderimage : String
        var ordername : String
        var orderprice : Int
        var orderitem1 : String
        var orderitem2 : String
        
    }
    var itemorder : [order]=[order]()
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemorder.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let ordercell = collectionView.dequeueReusableCell(withReuseIdentifier: "ordercollectioncell", for: indexPath)  as! orderCollectionViewCell
        ordercell.orderImage.image = UIImage(named:itemorder[indexPath.row].orderimage)
        ordercell.orderlabel1.text = itemorder[indexPath.row].ordername
        ordercell.orderlabel2.text = "\u{20B9}"+String(itemorder[indexPath.row].orderprice)
        ordercell.orderlabel3.text = itemorder[indexPath.row].orderitem1
        ordercell.orderlabel4.text = itemorder[indexPath.row].orderitem2
        ordercell.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        ordercell.layer.shadowOffset = CGSize(width: 0, height: 0.5)
        ordercell.layer.shadowRadius = 5.0
        ordercell.layer.shadowOpacity = 0.2
        ordercell.layer.masksToBounds = false
        ordercell.layer.shadowPath = UIBezierPath(roundedRect: ordercell.bounds, cornerRadius: ordercell.contentView.layer.cornerRadius).cgPath
        ordercell.layer.cornerRadius = 9.0
        ordercell.productnumberView.layer.borderWidth = 1
        ordercell.productnumberView.layer.borderColor = #colorLiteral(red: 0.05832260847, green: 0.05939355493, blue: 0.01971788704, alpha: 0.143166738)
        ordercell.productnumberView.layer.cornerRadius = 5.0
 return ordercell
    }
    override func awakeFromNib() {
        self.dataSource = self
        self.delegate  = self
        self.isScrollEnabled = false
    var orders1 = order(orderimage: "king image", ordername: "Burger king", orderprice: 50, orderitem1: "Fries", orderitem2: "Nuggest")
    var orders2 = order(orderimage: "pizza hut image", ordername: "Pizza hut", orderprice: 150, orderitem1: "Fries", orderitem2: "Nuggest")
//    var orders3 = order(orderimage: "chicking image", ordername: "Chicking", orderprice: 499, orderitem1: "fries", orderitem2: "Grilled")
//    var orders4 = order(orderimage: "kfc image", ordername: "KFC", orderprice: 599, orderitem1: "fries", orderitem2: "Grilled")
//    var orders5 = order(orderimage: "coffee image", ordername: "coffee", orderprice: 10, orderitem1: "sweets", orderitem2: "snakes")
        
      itemorder.append(orders1)
      itemorder.append(orders2)
//      itemorder.append(orders3)
//      itemorder.append(orders4)
//      itemorder.append(orders5)
        
    }
 
  

}
