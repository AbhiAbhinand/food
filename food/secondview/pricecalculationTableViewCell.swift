//
//  pricecalculationTableViewCell.swift
//  food
//
//  Created by Abhinand on 07/12/18.
//  Copyright © 2018 Abhinand. All rights reserved.
//

import UIKit

class pricecalculationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lbcarttotal: UILabel!
    @IBOutlet weak var lbTax: UILabel!
    @IBOutlet weak var lbDeliver: UILabel!
    @IBOutlet weak var lbPromodiscount: UILabel!
    @IBOutlet weak var lbCarttotalamount: UILabel!
    @IBOutlet weak var lbTaxamount: UILabel!
    @IBOutlet weak var lbDeliveryamount: UILabel!
    @IBOutlet weak var lbPromodiscountamount: UILabel!
    @IBOutlet weak var lblSubtotal: UILabel!
    @IBOutlet weak var lblSubtotalamount: UILabel!
    
    @IBOutlet weak var btProccedtocheckout: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        btProccedtocheckout.layer.cornerRadius = 7.0
        self.btProccedtocheckout.applyGradient(colours: [#colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1),#colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 0.5)], locations: [0.0, 0.3])
       btProccedtocheckout.clipsToBounds = true
        
//       lbCarttotalamount.text = "\u{20B9}"
//       lbPromodiscountamount.text = "\u{20B9}"
//       lbTaxamount.text = "\u{20B9}"
//       lbDeliveryamount.text = "\u{20B9}"
    }

}
extension UIButton {
    func applyGradient(colours: [UIColor]) -> Void {
        self.applyGradient(colours: colours, locations: nil)
        
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
        
    }
}
