//
//  orderCollectionViewCell.swift
//  food
//
//  Created by Abhinand on 23/11/18.
//  Copyright © 2018 Abhinand. All rights reserved.
//

import UIKit

class orderCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var orderImage: UIImageView!
    @IBOutlet weak var orderlabel1: UILabel!
    @IBOutlet weak var orderlabel2: UILabel!
    @IBOutlet weak var orderlabel3: UILabel!
    @IBOutlet weak var orderlabel4: UILabel!
    @IBOutlet weak var addbutton: UIButton!
    @IBOutlet weak var minusbutton: UIButton!
    @IBOutlet weak var orderNumberLabel: UILabel!
    
    @IBOutlet weak var productnumberView: UIView!
 
    @IBAction func btAdd(_ sender: UIButton) {
        var add = Int(orderNumberLabel.text!)
        add = add!+1
        orderNumberLabel.text = String(add!)
    }
    
    @IBAction func btMinus(_ sender: UIButton) {
        var minus = Int(orderNumberLabel.text!)
        minus = minus!-1
        if minus == 0{
           return
        }

        orderNumberLabel.text = String(minus!)
        
    }
    
}
