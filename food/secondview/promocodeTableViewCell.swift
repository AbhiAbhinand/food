//
//  promocodeTableViewCell.swift
//  food
//
//  Created by Abhinand on 29/11/18.
//  Copyright © 2018 Abhinand. All rights reserved.
//

import UIKit

class promocodeTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var txtPromocode: UITextField!
    
    @IBOutlet weak var btApply: UIButton!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
   btApply.layer.cornerRadius = 7.0
        self.btApply.applyGradient(colours: [#colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1),#colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 0.5)], locations: [0.0, 0.3])
        btApply.clipsToBounds = true
    }

    @IBAction func btApply(_ sender: UIButton) {
    }
    
    
}
extension UIButton {
    func applyGradients(colours: [UIColor]) -> Void {
        self.applyGradients(colours: colours, locations: nil)
        
    }
    
    func applyGradients(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
        
    }
}
