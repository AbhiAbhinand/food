//
//  OrderTableview.swift
//  food
//
//  Created by Abhinand on 03/12/18.
//  Copyright © 2018 Abhinand. All rights reserved.
//

import UIKit

class OrderTableview: UITableView , UITableViewDataSource,UITableViewDelegate {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        switch row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ordercell", for: indexPath) as! OrderTableViewCell
                return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "promocodecell", for: indexPath) as! promocodeTableViewCell
                return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "pricecalculationcell", for: indexPath) as! pricecalculationTableViewCell
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "pricecalculationcell", for: indexPath) as! pricecalculationTableViewCell
                return cell
        }
    
//        if row == 0 {
//
//            let cell = tableView.dequeueReusableCell(withIdentifier: "ordercell", for: indexPath) as! OrderTableViewCell
//            return cell
//        }
//        else {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "promocodecell") as! promocodeTableViewCell
//            return cell
//        }
    
}
    override func awakeFromNib() {
        self.dataSource = self
        self.delegate  = self
        
        //        self.estimatedRowHeight = 160
        //        self.rowHeight = UITableViewAutomaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 300
        }
        else if indexPath.row == 1 {
            return 100
        }
        else {
            return 300
            
        }
        
        
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */


}
