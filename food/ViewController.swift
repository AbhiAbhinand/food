//
//  ViewController.swift
//  food
//
//  Created by Abhinand on 16/11/18.
//  Copyright © 2018 Abhinand. All rights reserved.
//

import UIKit

class ViewController: UIViewController , UICollectionViewDataSource , UICollectionViewDelegate {
    
    struct items1 {
        var img :String
        var name :String
        var img1 :String
        var label1 :String
        var label2 :String
        var label3 :String
        var price :Int
    }
    struct items {
        var pic :String?
        var names :String?
    }
    var datasource : [items]=[items]()
    var lists : [items1]=[items1]()
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return collectionView==collectionview1 ? datasource.count : lists.count
    
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionview1 {
        let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "cell1", for: indexPath) as! collectionCollectionViewCell
            cell1.image1.image=UIImage(named:datasource[indexPath.row].pic!)
            cell1.name.text = datasource[indexPath.row].names!
            cell1.contentView.layer.cornerRadius = 5.0
            cell1.contentView.layer.borderWidth = 1.0
            cell1.contentView.layer.borderColor = UIColor.clear.cgColor
            cell1.contentView.layer.masksToBounds = false
            cell1.layer.shadowColor = UIColor.gray.cgColor
            cell1.layer.shadowOffset = CGSize(width: 0, height: 0.5)
            cell1.layer.shadowRadius = 4.0
            cell1.layer.shadowOpacity = 0.5
            cell1.layer.masksToBounds = false
            cell1.layer.shadowPath = UIBezierPath(roundedRect: cell1.bounds, cornerRadius: cell1.contentView.layer.cornerRadius).cgPath
         return cell1
        }
        else
        {
           let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "cell2", for: indexPath) as! collect
        
            cell2.image2.image=UIImage(named:lists[indexPath.row].img)
            cell2.lbname.text = lists[indexPath.row].name
        cell2.image3.image=UIImage(named: lists[indexPath.row].img1)
        cell2.lbprice.text="\u{20B9}"+String(lists[indexPath.row].price)
        cell2.lb1.text=lists[indexPath.row].label1
        cell2.lb2.text=lists[indexPath.row].label2
        cell2.lb3.text=lists[indexPath.row].label3
        cell2.lb1.layer.cornerRadius = 5.0
            cell2.lb1.layer.masksToBounds = true
            cell2.lb2.layer.cornerRadius = 5.0
            cell2.lb2.layer.masksToBounds = true
            cell2.lb3.layer.cornerRadius = 5.0
            cell2.lb3.layer.masksToBounds = true
            cell2.image2.roundCorners(corners:[.topLeft,.topRight], radius: 10.0)
       // cell2.contentView.layer.cornerRadius = 10.0
        cell2.contentView.layer.borderWidth = 1.0
        cell2.contentView.layer.borderColor = UIColor.clear.cgColor
            cell2.layer.cornerRadius = 10
        //cell2.contentView.layer.masksToBounds = false
        cell2.layer.shadowColor = UIColor.gray.cgColor
        cell2.layer.shadowOffset = CGSize(width: 0, height: 0.5)
        cell2.layer.shadowRadius = 4.0
        cell2.layer.shadowOpacity = 0.5
        cell2.layer.masksToBounds = false
        cell2.layer.shadowPath = UIBezierPath(roundedRect: cell2.bounds, cornerRadius: cell2.contentView.layer.cornerRadius).cgPath
        return cell2
        }
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionview1.dataSource = self
        collectionview1.delegate  = self
        collectionview2.dataSource = self
        collectionview2.delegate = self
        
    datasource.append(ViewController.items.init(pic: "1", names: "Burger"))
        var item1 = items(pic: "2", names: "pizza")
        var item2 = items(pic: "3", names: "Kfc")
        var item3 = items(pic: "4", names: "coffe")
        var item4 = items(pic: "5", names: "Soft drinks")
        var item5 = items(pic: "6", names: "chicking")
        
       var shop1 = items1(img: "king image", name: "Burger king", img1: "king logo", label1: "Burgers", label2: "Fries", label3: "Nuggest", price: 50)
       var  shop2 = items1(img: "pizza hut image", name: "Pizza hut", img1: "pizza hut logo", label1: "Pizza", label2: "Fries", label3: "Nuggest", price: 150)
       var shop3 = items1(img: "chicking image", name: "Chicking", img1: "chicking logo", label1: "chicken", label2: "fries", label3: "grilled", price: 499)
        var shop4 = items1(img: "kfc image", name: "KFC", img1: "kfc logo", label1: "chicken", label2: "fries", label3: "Grilled", price: 599)
        var shop5 = items1(img: "coffee image", name: "Sogbu", img1: "coffee logo", label1: "Biscut", label2: "sweetes", label3: "snakes", price: 10)
        
        
        datasource.append(item1)
        datasource.append(item2)
        datasource.append(item3)
        datasource.append(item4)
        datasource.append(item5)
        
        lists.append(shop1)
        lists.append(shop2)
        lists.append(shop3)
        lists.append(shop4)
        lists.append(shop5)
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var collectionview1: UICollectionView!
    @IBOutlet weak var collectionview2: UICollectionView!
    
   
    

}
extension UIImageView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
