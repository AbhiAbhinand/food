//
//  catagarycollection.swift
//  food
//
//  Created by Abhinand on 21/11/18.
//  Copyright © 2018 Abhinand. All rights reserved.
//

import UIKit

class catagarycollection: UICollectionView , UICollectionViewDataSource,UICollectionViewDelegate {
    
    
    struct items {
        var pic :String?
        var names :String?
    }
    var datasource : [items]=[items]()
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      return datasource.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "cell1", for: indexPath) as! collectionCollectionViewCell
        cell1.image1.image=UIImage(named:datasource[indexPath.row].pic!)
        cell1.name.text = datasource[indexPath.row].names!
        cell1.contentView.layer.cornerRadius = 5.0
        cell1.contentView.layer.borderWidth = 1.0
        cell1.contentView.layer.borderColor = UIColor.clear.cgColor
        cell1.contentView.layer.masksToBounds = false
        cell1.layer.shadowColor = UIColor.gray.cgColor
        cell1.layer.shadowOffset = CGSize(width: 0, height: 0.5)
        cell1.layer.shadowRadius = 1.0
        cell1.layer.shadowOpacity = 0.5
        cell1.layer.masksToBounds = false
        cell1.layer.shadowPath = UIBezierPath(roundedRect: cell1.bounds, cornerRadius: cell1.contentView.layer.cornerRadius).cgPath
        return cell1
        
    }
     

    


    override func awakeFromNib() {
        self.dataSource = self
        self.delegate  = self
        var item = items(pic: "1", names: "Burger king")
        var item1 = items(pic: "2", names: "pizza")
        var item2 = items(pic: "3", names: "KFC")
        var item3 = items(pic: "4", names: "coffe")
        var item4 = items(pic: "5", names: "Soft Drinks")
        var item5 = items(pic: "6", names: "chicking")
        datasource.append(item)
        datasource.append(item1)
        datasource.append(item2)
        datasource.append(item3)
        datasource.append(item4)
        datasource.append(item5)
        
    }


}
