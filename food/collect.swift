//
//  collect.swift
//  food
//
//  Created by Abhinand on 19/11/18.
//  Copyright © 2018 Abhinand. All rights reserved.
//

import UIKit

class collect: UICollectionViewCell {
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var image3: UIImageView!
    @IBOutlet weak var lbname: UILabel!
    @IBOutlet weak var lb1: UILabel!
    @IBOutlet weak var lb2: UILabel!
    @IBOutlet weak var lb3: UILabel!
    @IBOutlet weak var lbprice: UILabel!
    
    func populate(with list: items1) {
      image2.image=UIImage(named:list.img)
      lbname.text = list.name
      image3.image=UIImage(named: list.img1)
      lbprice.text="\u{20B9}"+String(list.price)
      lb1.text=list.label1
      lb2.text=list.label2
      lb3.text=list.label3
      lb1.layer.cornerRadius = 5.0
      lb1.layer.masksToBounds = true
      lb2.layer.cornerRadius = 5.0
      lb2.layer.masksToBounds = true
      lb3.layer.cornerRadius = 5.0
      lb3.layer.masksToBounds = true
      image2.roundCorners(corners:[.topLeft,.topRight], radius: 10.0)
        // cell2.contentView.layer.cornerRadius = 10.0
      contentView.layer.borderWidth = 1.0
      contentView.layer.borderColor = UIColor.clear.cgColor
      layer.cornerRadius = 10
        //cell2.contentView.layer.masksToBounds = false
    
      layer.shadowColor = UIColor.gray.cgColor
      layer.shadowOffset = CGSize(width: 0, height: 0.5)
      layer.shadowRadius = 10.0
      layer.shadowOpacity = 0.3
      layer.masksToBounds = false
      layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
    }
   
        
    }


